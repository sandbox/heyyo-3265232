<?php

namespace Drupal\url_inspector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure URL Inspector settings for this site.
 */
final class UrlInspectorViewsSettingsForm extends ConfigFormBase {

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UrlInspectorSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    EntityTypeManagerInterface $entity_type_manager,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'url_inspector_url_inspector_views_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['url_inspector.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $selected_views = $this
      ->config('url_inspector.settings')
      ->get('views_enabled') ?: [];

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => [
        'view' => $this->t('View'),
        'display' => $this->t('Display'),
        'url' => $this->t('Url'),
      ],
      '#options' => $this->getEnabledDisplaysOptions(),
      '#empty' => $this->t('No page views found'),
      '#default_value' => array_combine($selected_views, $selected_views),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $selected_views = array_filter($form_state->getValue('table'));
    $selected_views = array_keys($selected_views);

    $this->config('url_inspector.settings')
      ->set('views_enabled', $selected_views)
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Prepares array of the options for the table.
   *
   * @return array
   *   Array of the views displays, formatted for the tableselect.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function getEnabledDisplaysOptions() {
    $options = [];
    $views_ids = $this->entityTypeManager
      ->getStorage('view')->getQuery()
      ->condition('status', TRUE)
      ->condition("display.*.display_plugin", ['page'], 'IN')
      ->execute();

    if (!$views_ids) {
      return $options;
    }

    $views = $this->entityTypeManager->getStorage('view')
      ->loadMultiple($views_ids);

    foreach ($views as $view) {
      foreach ($view->get('display') as $id => $display) {
        $enabled = !empty($display['display_options']['enabled'])
          || !array_key_exists('enabled', $display['display_options']);

        if ($enabled && $display['display_plugin'] === 'page') {
          $options[$view->id() . '.' . $id] = [
            'view' => $view->label(),
            'display' => $display['display_title'],
            'url' => $display['display_options']['path'],
          ];
        }
      }
    }

    asort($options);
    return $options;
  }

}
