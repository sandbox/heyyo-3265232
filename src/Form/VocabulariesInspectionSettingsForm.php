<?php

namespace Drupal\url_inspector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure URL Inspector settings for the vocabularies.
 */
class VocabulariesInspectionSettingsForm extends ConfigFormBase {

  /**
   * ModuleHandler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * DatabaseConnection service.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $dbConnection;

  /**
   * Constructs ContentTypesInspectionSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    ModuleHandlerInterface $module_handler,
    EntityTypeManagerInterface $entity_type_manager,
    Connection $database_connection,
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->moduleHandler = $module_handler;
    $this->entityTypeManager = $entity_type_manager;
    $this->dbConnection = $database_connection;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): VocabulariesInspectionSettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('module_handler'),
      $container->get('entity_type.manager'),
      $container->get('database'),
      $container->has('typed.config_manager') ? $container->get('typed_config_manager') : NULL
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['url_inspector.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'url_inspector_vocabularies_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    if (!$this->moduleHandler->moduleExists('taxonomy')) {
      $form['taxonomy_module_disabled'] = [
        '#type' => 'item',
        '#description' => $this->t('Taxonomy module is disabled. No configuration options are available.'),
      ];
      return $form;
    }
    $form['vocabularies'] = [
      '#type' => 'tableselect',
      '#header' => [
        'vocabulary' => $this->t('Vocabulary'),
        'terms_count' => $this->t('Terms count'),
      ],
      '#options' => $this->getVocabularies(FALSE),
      '#empty' => $this->t('No vocabularies found.'),
      '#default_value' => $this->getVocabularies(),
    ];
    $form['#attached']['library'][] = 'url_inspector/drupal.url_inspector.selected_count';
    // Prepare an element to display count of applicable entities.
    $form['selected_entities_count'] = [
      '#type' => 'html_tag',
      '#tag' => 'span',
      '#attributes' => [
        'class' => ['selected-entities-count'],
      ],
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $selected_vocabularies = array_filter($form_state->getValue('vocabularies'));

    $this->config('url_inspector.settings')
      ->set('vocabularies', $selected_vocabularies)
      ->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Returns vocabularies options for tableselect element.
   *
   * @throws \Exception
   */
  public function getVocabularies(bool $only_configured = TRUE): array {
    if ($only_configured) {
      return $this->configFactory->get('url_inspector.settings')
        ->get('vocabularies') ?? [];
    }

    $all_vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();
    $nodes_counts_query = $this->dbConnection->select('taxonomy_term_data', 'ttd');
    $nodes_counts_query->fields('ttd', ['vid']);
    $nodes_counts_query->addExpression('count(vid)', 'vid_term_count');
    $nodes_counts_query->groupBy('ttd.vid');
    $query_result = $nodes_counts_query->execute()
      ->fetchAllAssoc('vid', \PDO::FETCH_ASSOC);
    return array_map(static function ($node_type) use ($query_result) {
      return [
        'vocabulary' => $node_type->label(),
        'terms_count' => $query_result[$node_type->id()]['vid_term_count'] ?? 0,
      ];
    }, $all_vocabularies);
  }

}
