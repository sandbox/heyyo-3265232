<?php

namespace Drupal\url_inspector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RequestContext;
use Drupal\Core\Url;
use Drupal\google_api_client\Entity\GoogleApiServiceClient;
use Drupal\url_inspector\GoogleSearchConsoleAPI;
use Drupal\url_inspector\VerdictType;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class UrlInspectorSettingsForm. The config form for the url_inspector module.
 */
class UrlInspectorSettingsForm extends ConfigFormBase {

  /**
   * The Google Search Console API service name.
   *
   * Used in the service field of the GoogleApiServiceClient entity.
   *
   * @var string
   */
  const SEARCH_CONSOLE_API_SERVICE = 'webmasters';

  /**
   * The request context.
   *
   * @var \Drupal\Core\Routing\RequestContext
   */
  protected $requestContext;

  /**
   * The Google Search Console API service.
   *
   * @var \Drupal\url_inspector\GoogleSearchConsoleAPI
   */
  protected $googleSearchConsoleAPI;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * UrlInspectorSettingsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory for the form.
   * @param \Drupal\Core\Config\TypedConfigManagerInterface $typedConfigManager
   *   The typed config manager.
   * @param \Drupal\Core\Routing\RequestContext $request_context
   *   The request context.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger service.
   * @param \Drupal\url_inspector\GoogleSearchConsoleAPI $google_search_console_api
   *   The Google search console API service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    TypedConfigManagerInterface $typedConfigManager,
    RequestContext $request_context,
    GoogleSearchConsoleAPI $google_search_console_api,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger
  ) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->requestContext = $request_context;
    $this->googleSearchConsoleAPI = $google_search_console_api;
    $this->entityTypeManager = $entity_type_manager;
    $this->setMessenger($messenger);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('router.request_context'),
      $container->get('url_inspector.google_search_console_api'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'url_inspector.settings',
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'url_inspector_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('url_inspector.settings');

    $form['site_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Current website URL:'),
      '#default_value' => $this->requestContext->getCompleteBaseUrl(),
      '#size' => 60,
      '#description' => $this->t('URL using for inspection with Google Search Console API.'),
      '#disabled' => TRUE,
    ];

    $google_service_accounts = $this->getAvailableGoogleApiServices() ?? [];
    $google_service_account_exists = FALSE;

    // If there are no Google service accounts that use the Search Console API,
    // we show an error message.
    if (empty($google_service_accounts)) {
      $this->messenger()
        ->addError($this->t('You need to create a Google service account to use Google Search Console API. You can create it <a href=":url">here</a>.', [
          ':url' => $this->getGoogleApiClientServiceAddFormUrl(),
        ]));
    }
    else {
      $google_service_account_id = $config->get('google_service_account') ?? NULL;

      if ($google_service_account_id) {
        if (!array_key_exists($google_service_account_id, $google_service_accounts)) {
          $this->messenger()
            ->addError($this->t('The Google service account that is used for Google Search Console API does not exist. Please, choose another one from the list below.'));
        }
        else {
          $google_service_account = $google_service_accounts[$google_service_account_id];
          $google_service_account_exists = TRUE;
          $this->messenger()
            ->addStatus($this->t('The Google service account <strong>@name</strong> is used for Google Search Console API.', [
              '@name' => $google_service_account->label(),
            ]));
        }
      }
      else {
        $this->messenger()
          ->addWarning($this->t('There is no Google service account that is used for Google Search Console API. Please, choose one from the list below.'));
      }
    }

    $form['google_service_account'] = [
      '#type' => 'select',
      '#title' => $this->t('Google service account:'),
      '#options' => $this->getGoogleApiServiceOptions($google_service_accounts),
      '#description' => $this->t('The Google service account used for Google Search Console API. Choose another one if you want.'),
      '#disabled' => empty($google_service_accounts),
      '#default_value' => isset($google_service_account) ? $google_service_account->id() : NULL,
      '#required' => TRUE,
    ];

    $form['cron_inspection'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable inspection by Cron'),
      '#disabled' => !$google_service_account_exists,
      '#default_value' => $config->get('cron_inspection') ?? 0,
    ];
    $form['cron_inspection_time_container'] = [
      '#type' => 'container',
      '#title' => $this->t('By default - use a time range between 00:00:00 and 1 am.'),
      '#states' => [
        'visible' => [
          ':input[name="cron_inspection"]' => [
            'checked' => TRUE,
          ],
        ],
      ],
    ];
    $form['cron_inspection_time_container']['explanation'] = [
      '#type' => 'item',
      '#title' => $this->t('By default - a time range between 00:00:00 and 1 am will be used.'),
    ];
    $form['cron_inspection_time_container']['cron_inspection_time_start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Inspection time start'),
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime($config->get('cron_inspection_time_start') ?? '00:00:00')),
      '#size' => 20,
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
      '#date_time_format' => 'H:i',
    ];
    $form['cron_inspection_time_container']['cron_inspection_time_end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Inspection time end'),
      '#default_value' => DrupalDateTime::createFromTimestamp(strtotime($config->get('cron_inspection_time_end') ?? '00:00:00')),
      '#size' => 20,
      '#date_date_element' => 'none',
      '#date_time_element' => 'time',
      '#date_time_format' => 'H:i',
    ];

    $form['test_inspection_wrapper'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'test-inspection-wrapper',
      ],
    ];

    $form['test_inspection_wrapper']['summary'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['']],
    ];

    $form['test_inspection_wrapper']['response'] = [
      '#type' => 'container',
      '#attributes' => ['class' => ['fieldgroup']],
    ];

    $form['test_inspection_wrapper']['actions']['test'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test index status'),
      '#button_type' => 'primary',
      '#disabled' => !$google_service_account_exists,
      '#submit' => [[$this, 'testIndex']],
      '#ajax' => [
        'callback' => '::testUrlInspectionCallback',
        'wrapper' => 'test-inspection-wrapper',
      ],
      '#weight' => 1,
    ];

    // @todo Add needed fields on the form.
    return parent::buildForm($form, $form_state);
  }

  /**
   * Ajax callback for the Test Inspection submit.
   */
  public function testUrlInspectionCallback(array &$form, FormStateInterface $form_state) {
    return $form['test_inspection_wrapper'];
  }

  /**
   * Returns the Google service accounts that use the Search Console API.
   *
   * @return \Drupal\google_api_client\Entity\GoogleApiServiceClient[]|null
   *   The Google service accounts that use the Search Console API or NULL if
   *   there is no one.
   */
  protected function getAvailableGoogleApiServices(): ?array {
    $google_service_account_storage = $this->entityTypeManager
      ->getStorage('google_api_service_client');
    $google_service_accounts = $google_service_account_storage->loadMultiple();

    if (empty($google_service_accounts)) {
      return NULL;
    }
    else {
      // Filter the Google service accounts by the Search Console API service.
      $google_service_accounts = array_filter($google_service_accounts, static function (GoogleApiServiceClient $google_service_account) {
        return array_key_exists(static::SEARCH_CONSOLE_API_SERVICE, $google_service_account->getServices());
      });
    }

    return $google_service_accounts;
  }

  /**
   * Returns the options for the Google service account select field.
   *
   * @param \Drupal\google_api_client\Entity\GoogleApiServiceClient[] $google_service_accounts
   *   The Google service accounts.
   *
   * @return array
   *   The options for the Google service account select field.
   */
  protected function getGoogleApiServiceOptions(array $google_service_accounts): array {
    $google_service_options = [];
    foreach ($google_service_accounts as $google_service_account) {
      $google_service_options[$google_service_account->id()] = $google_service_account->label();
    }
    return $google_service_options;
  }

  /**
   * Returns the Google service account entity by its ID.
   *
   * @param string $service_id
   *   The ID of the Google service account.
   *
   * @return \Drupal\google_api_client\Entity\GoogleApiServiceClient|null
   *   The Google service account entity or NULL if it does not exist.
   */
  protected function loadGoogleApiServiceClient(string $service_id): ?GoogleApiServiceClient {
    $google_service_account_storage = $this->entityTypeManager
      ->getStorage('google_api_service_client');

    /** @var \Drupal\google_api_client\Entity\GoogleApiServiceClient $google_service_api_client */
    $google_service_api_client = $google_service_account_storage->load($service_id);

    return $google_service_api_client;
  }

  /**
   * Returns the URL of the Google service account add form.
   *
   * @return string
   *   The URL of the Google service account add form.
   */
  protected function getGoogleApiClientServiceAddFormUrl(): string {
    return Url::fromRoute('entity.google_api_service_client.add_form')->toString();
  }

  /**
   * Make a test request to Google Search Console API for index URL.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function testIndex(array &$form, FormStateInterface $form_state): void {
    $google_service_account = $form['google_service_account']['#value'] ?? NULL;
    $url = $form_state->getValue('site_url');
    $domain = parse_url($url, PHP_URL_HOST);
    $domain = "sc-domain:" . $domain;

    try {
      $response = $this->googleSearchConsoleAPI->getIndexStatus($url, $domain, $google_service_account);
      // @see https://developers.google.com/webmaster-tools/v1/urlInspection.index/UrlInspectionResult#Verdict
      // @todo Perhaps we can move this logic to the service class.
      $result = $response
        ->getInspectionResult()
        ->getIndexStatusResult();

      $response_data = json_encode($result->toSimpleObject(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);

      $verdict = $result->getVerdict();
      $response_message = $this->t(
        '<strong>@url - @status (@indexing_state)</strong>',
        [
          '@url' => $url,
          '@status' => $verdict,
          '@indexing_state' => $result->getIndexingState(),
        ]
      );
      if ($verdict === VerdictType::PASS->name) {
        $status_class = 'status';
      }
      else {
        $status_class = 'warning';
      }
    }
    catch (\Exception $e) {
      $response_data = $e->getMessage();
      $status_class = 'error';
      $response_message = '';
      foreach ($e->getErrors() as $error) {
        $response_message .= $error['reason'] . ' - ' . $error['message'] . "\r\n";
      }
    }

    $form['test_inspection_wrapper']['summary']['#attributes']['class'][] = 'messages';
    $form['test_inspection_wrapper']['summary']['#attributes']['class'][] = 'messages--' . $status_class;
    $form['test_inspection_wrapper']['summary'][] = [
      '#type' => 'container',
      '#attributes' => [
        'class' => ['messages-list__item'],
      ],
      'text' => [
        '#type' => 'markup',
        '#markup' => $response_message,
      ],
    ];

    $form['test_inspection_wrapper']['response']['#attributes']['class'][] = 'messages';
    $form['test_inspection_wrapper']['response'][] = [
      '#type' => 'markup',
      '#markup' => '<pre>' . $response_data . '</pre>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('url_inspector.settings')
      ->set('cron_inspection', $form_state->getValue('cron_inspection'))
      ->set('cron_inspection_time_start', (string) $form_state->getValue('cron_inspection_time_start')->format('H:i:s'))
      ->set('cron_inspection_time_end', (string) $form_state->getValue('cron_inspection_time_end')->format('H:i:s'))
      ->set('google_service_account', $form_state->getValue('google_service_account'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
