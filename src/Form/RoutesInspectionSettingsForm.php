<?php

namespace Drupal\url_inspector\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\AdminContext;
use Drupal\Core\Routing\RouteProviderInterface;
use Drupal\jsonapi\Routing\Routes;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a setting form to configure inspection of custom routes.
 */
class RoutesInspectionSettingsForm extends ConfigFormBase {

  /**
   * Constructs RoutesInspectionSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    private readonly RouteProviderInterface $routeProvider,
    private readonly AdminContext $adminContext,
    private readonly ModuleHandlerInterface $moduleHandler,
    $typedConfigManager = NULL,
  ) {
    parent::__construct($configFactory, $typedConfigManager);
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): RoutesInspectionSettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('router.route_provider'),
      $container->get('router.admin_context'),
      $container->get('module_handler'),
      $container->has('config.typed') ? $container->get('config.typed') : NULL
    );
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames(): array {
    return ['url_inspector.settings'];
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'url_inspector_routes_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['routes'] = [
      '#type' => 'tableselect',
      '#header' => [
        'route_name' => $this->t('Route'),
        'route_path' => $this->t('Route path'),
      ],
      '#options' => $this->getRoutes(FALSE),
      '#empty' => $this->t('No routes found.'),
      '#default_value' => $this->getRoutes(),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * Returns an array of routes available for configuration.
   */
  public function getRoutes(bool $only_configured = TRUE): array {
    if ($only_configured) {
      return $this->configFactory->get('url_inspector.settings')
        ->get('routes') ?? [];
    }
    $routes = [];
    // Exclude routes from the list with no-route meaning.
    $exclude_list = [
      '<current>',
      '<none>',
      '<nolink>',
      '<button>',
    ];
    $check_for_json_api = $this->moduleHandler->moduleExists('jsonapi');
    foreach ($this->routeProvider->getAllRoutes() as $name => $route) {
      if (in_array($name, $exclude_list, TRUE)) {
        continue;
      }
      // Admin routes can't be crawled by google (or it just crawls it as 403
      // page) so we don't really need it to be configured.
      if ($this->adminContext->isAdminRoute($route)
        || (!empty($requirements = $route->getRequirements())
          && isset($requirements['_permission'])
          && str_contains($requirements['_permission'], 'administer'))) {
        continue;
      }

      // Be sure that routes that does not accept GET method are excluded.
      if (!in_array('GET', $route->getMethods(), TRUE)) {
        continue;
      }

      // Exclude JSON:API routes as well.
      if ($check_for_json_api && Routes::isJsonApiRequest($route->getDefaults())) {
        continue;
      }

      // Nodes/terms/views handled separately so remove them from configuration
      // as well.
      if ($name === 'entity.node.canonical' || $name === 'entity.taxonomy_term.canonical'
        || str_starts_with($name, 'view.')) {
        continue;
      }
      $routes[str_replace('.', '|', $name)] = [
        'route_name' => $name,
        'route_path' => $route->getPath(),
      ];
    }
    return $routes;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $selected_routes = array_filter($form_state->getValue('routes'));
    // Prepare values as a correct route name.
    $values = [];
    foreach ($selected_routes as $name) {
      $values[] = str_replace('|', '.', $name);
    }
    $this->config('url_inspector.settings')
      ->set('routes', array_combine(array_keys($selected_routes), $values))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
