<?php

namespace Drupal\url_inspector;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Google\Service\SearchConsole\InspectUrlIndexResponse;

/**
 * Contains methods to work with entity forms.
 */
class FormHelper {

  use StringTranslationTrait;

  /**
   * Url inspection operations manager.
   *
   * @var \Drupal\url_inspector\UrlInspectionOperationsManager
   */
  protected UrlInspectionOperationsManager $urlInspectionOperationsManager;

  /**
   * Constructs FormHelper object.
   */
  public function __construct(
    UrlInspectionOperationsManager $url_inspection_operations_manager,
  ) {
    $this->urlInspectionOperationsManager = $url_inspection_operations_manager;
  }

  /**
   * Alters entity form adding an extra tab.
   */
  public function alterEntityForm(&$form, $entity): void {
    $last_inspection = $this->urlInspectionOperationsManager->getLastInspection($entity);
    $last_inspection_data = [
      'last_crawled' => $this->t('This page was not inspected yet or has no data in this field.'),
      'verdict_status' => $this->t('This page was not inspected yet or has no data in this field.'),
      'detailed_info' => $this->t('This page was not inspected yet or has no data in this field.'),
    ];
    if ($last_inspection instanceof UrlInspectionInterface) {
      if (!$last_inspection->get('crawled_date')->isEmpty()) {
        $last_inspection_data['last_crawled'] = date('Y-m-d, h:i:s', $last_inspection->get('crawled_date')
          ->getString());
      }
      foreach (['verdict_status', 'detailed_info'] as $field_name) {
        if (!$last_inspection->get($field_name)->isEmpty()) {
          $last_inspection_data[$field_name] = $last_inspection->get($field_name)->getString();
        }
      }
    }
    $form['url_inspector'] = [
      '#type' => 'details',
      '#title' => $this->t('URL Inspection'),
      '#group' => 'advanced',
      '#weight' => '100',
      '#attributes' => [
        'id' => 'url-inspection-details',
      ],
    ];
    $form['url_inspector']['container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'url-inspection-container',
      ],
    ];
    $form['url_inspector']['container']['last_crawled'] = [
      '#type' => 'item',
      '#title' => $this->t('Last Crawled'),
      '#markup' => $last_inspection_data['last_crawled'],
    ];
    $form['url_inspector']['container']['verdict_status'] = [
      '#type' => 'item',
      '#title' => $this->t('Verdict'),
      '#markup' => $last_inspection_data['verdict_status'] instanceof TranslatableMarkup ? $last_inspection_data['verdict_status'] : VerdictType::from((int) $last_inspection_data['verdict_status'])->name,
    ];
    $form['url_inspector']['container']['detailed_info'] = [
      '#type' => 'item',
      '#title' => $this->t('Detailed info'),
      '#markup' => $last_inspection_data['detailed_info'],
    ];
    $form['url_inspector']['container']['check_now'] = [
      '#type' => 'submit',
      '#value' => $this->t('Run inspection'),
      '#description' => $this->t('Manually runs an inspection for the entity. It could take a while.'),
      '#submit' => [static::class . '::runInspection'],
      '#ajax' => [
        'callback' => [static::class, 'rebuildInspectionForm'],
        'wrapper' => 'url-inspection-container',
      ],
    ];
  }

  /**
   * Runs inspection using Google API.
   *
   * @throws \JsonException
   */
  public static function runInspection(&$form, FormStateInterface $form_state): void {
    $service_account = \Drupal::configFactory()->get('url_inspector.settings')->get('google_service_account');
    $entity = $form_state->getFormObject()->getEntity();
    $entity_url = $entity->toUrl('canonical')->toString();
    $host = \Drupal::requestStack()->getCurrentRequest()->getSchemeAndHttpHost();
    $domain = parse_url($host, PHP_URL_HOST);
    $domain = "sc-domain:" . $domain;
    $result = \Drupal::service('url_inspector.google_search_console_api')->getIndexStatus($host . $entity_url, $domain, $service_account);
    $operations_manager = \Drupal::service('url_inspector.url_inspection.operations_manager');
    if ($result instanceof InspectUrlIndexResponse) {
      $index_status_result = $result->getInspectionResult()->getIndexStatusResult();
      $data = [
        'bundle' => 'url_inspection_type',
        'label' => 'Inspection: ' . $entity->getEntityTypeId() . '|' . $entity->bundle() . '|' . $entity->id(),
        'entity_identifier' => serialize([
          'entity_type_id' => $entity->getEntityTypeId(),
          'entity_id' => $entity->id(),
        ]),
        'verdict_status' => VerdictType::fromName($index_status_result->getVerdict()),
        'crawled_date' => strtotime($index_status_result->getLastCrawlTime()),
        'detailed_info' => json_encode($index_status_result->toSimpleObject(), JSON_THROW_ON_ERROR | JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES),
      ];
      if ($operations_manager->inspectionExists($data['entity_identifier'])) {
        $operations_manager->updateInspection($data);
      }
      else {
        $operations_manager->createInspection($data);
      }
    }
    $form_state->setRebuild();
  }

  /**
   * Rebuilds inspection form.
   */
  public static function rebuildInspectionForm($form, FormStateInterface $form_state): array {
    return $form['url_inspector']['container'];
  }

}
