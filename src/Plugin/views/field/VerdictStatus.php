<?php

namespace Drupal\url_inspector\Plugin\views\field;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Component\Render\MarkupInterface;
use Drupal\node\NodeInterface;
use Drupal\url_inspector\Entity\UrlInspection;
use Drupal\url_inspector\UrlInspectionOperationsManager;
use Drupal\url_inspector\VerdictType;
use Drupal\views\Attribute\ViewsField;
use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Field handler to present the verdict status of an entity.
 *
 * @ingroup views_field_handlers
 */
#[ViewsField("verdict_status")]
class VerdictStatus extends FieldPluginBase {

  /**
   * Url inspector operations manager service.
   *
   * @var \Drupal\url_inspector\UrlInspectionOperationsManager
   */
  protected UrlInspectionOperationsManager $urlInspectionOperationsManager;

  /**
   * Constructs VerdictStatus object.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    UrlInspectionOperationsManager $url_inspection_operations_manager,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->urlInspectionOperationsManager = $url_inspection_operations_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition,
  ) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('url_inspector.url_inspection.operations_manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function query(): void {
    // No query needed.
  }

  /**
   * {@inheritdoc}
   */
  public function render(ResultRow $values): MarkupInterface|string {
    if (isset($values->_entity) && $values->_entity instanceof NodeInterface) {
      $last_inspection_entity = $this->urlInspectionOperationsManager->getLastInspection($values->_entity);
      if (!$last_inspection_entity instanceof UrlInspection) {
        return '';
      }
      $last_inspection_verdict_value = (int) $last_inspection_entity->get('verdict_status')->getString();
      $markup = new FormattableMarkup('
        <span class="verdict-status--icon @verdict-status-color" title="Status: @verdict-status-text"></span>
      ', [
        '@verdict-status-color' => VerdictType::getColor($last_inspection_verdict_value),
        '@verdict-status-text' => VerdictType::tryFrom($last_inspection_verdict_value)?->name,
      ]);
      $markup_render_array = [
        '#type' => 'html_tag',
        '#tag' => 'div',
        '#value' => $markup,
        '#attached' => [
          'library' => ['url_inspector/drupal.url_inspector.verdict_status'],
        ],
      ];
      return $this->renderer->render($markup_render_array);
    }
    return '';
  }

}
