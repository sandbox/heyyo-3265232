<?php

namespace Drupal\url_inspector\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;
use Drupal\url_inspector\QueueHelper;
use Drupal\url_inspector\UrlInspectionOperationsManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Collect information about views URLs.
 */
final class UrlInspectorArgumentCollector implements EventSubscriberInterface {

  /**
   * Constructs an UrlInspectorViewsUrlsSubscriber object.
   */
  public function __construct(
    private readonly EntityTypeManagerInterface $entityTypeManager,
    private readonly RouteMatchInterface $routeMatch,
    private readonly ConfigFactoryInterface $configFactory,
    private readonly QueueHelper $queueHelper,
    private readonly RequestStack $requestStack,
    private readonly UrlInspectionOperationsManager $operationsManager,
  ) {
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    $events[KernelEvents::TERMINATE] = 'onTerminate';
    return $events;
  }

  /**
   * Collect information about views URLs.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   Terminate event object.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Exception
   */
  public function onTerminate(TerminateEvent $event): void {
    // Only successful requests are interesting.
    if ($event->getResponse()->isSuccessful()) {
      // There could be different inspection types like view page, node, term
      // or a route that is configured to be tracked.
      // Try to get view id and display id as a first processed type.
      // Get view ID from route.
      $view_id = $this->routeMatch->getParameter('view_id');
      // Get display ID from route.
      $display_id = $this->routeMatch->getParameter('display_id');
      if (!empty($view_id) && !empty($display_id) && $this->operationsManager->shouldTrackView($view_id, $display_id)) {
        $this->processView($view_id, $display_id);
        return;
      }

      // Check for nodes now.
      if (!empty($node = $this->routeMatch->getParameter('node'))
      && $this->operationsManager->shouldTrackNode($node)) {
        $this->processNode($node);
        return;
      }

      // Check for term.
      if (!empty($term = $this->routeMatch->getParameter('taxonomy_term'))
      && $this->operationsManager->shouldTrackTerm($term)) {
        $this->processTerm($term);
        return;
      }

      $route_name = $this->routeMatch->getRouteName();
      // And check whether it's a request to the configured route.
      if (!empty($route_name) && $this->operationsManager->shouldTrackRoute($route_name)) {
        $this->processRoute($route_name);
      }
    }

  }

  /**
   * Processes taxonomy term.
   *
   * @throws \Exception
   */
  protected function processTerm(TermInterface $term): void {
    $this->queueHelper->queueUnique([
      'entity_type' => $term->getEntityTypeId(),
      'entity_bundle' => $term->bundle(),
      'entity_id' => $term->id(),
      'path' => $term->toUrl()->setAbsolute(FALSE)->toString(),
    ], 'terms_inspections');
  }

  /**
   * Process configured route.
   *
   * @throws \Exception
   */
  protected function processRoute(string $route_name): void {
    $request = $this->requestStack->getCurrentRequest();
    if ($request === NULL) {
      return;
    }
    $this->queueHelper->queueUnique([
      'entity_type' => 'route',
      'entity_bundle' => 'route',
      'entity_id' => $route_name,
      'path' => $request->getPathInfo(),
    ], 'routes_inspections');
  }

  /**
   * Process view page.
   *
   * @throws \Exception
   */
  protected function processView($view_id, $display_id): void {
    /** @var \Drupal\views\ViewEntityInterface $view_entity */
    $view_entity = $this->entityTypeManager
      ->getStorage('view')
      ->load($view_id);

    if (!$view_entity) {
      return;
    }

    // Get a set of view arguments and try to add them to the index.
    $view = $view_entity->getExecutable();

    $this->queueHelper->queueUnique([
      'path' => $view->getPath(),
      'entity_id' => $view_id . '|' . $display_id,
      'entity_bundle' => $display_id,
      'entity_type' => 'view',
    ], 'views_inspections');

    // Destroy a view instance.
    $view->destroy();
  }

  /**
   * Process node page.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  protected function processNode(NodeInterface $node): void {
    $this->queueHelper->queueUnique([
      'entity_type' => $node->getEntityTypeId(),
      'entity_bundle' => $node->bundle(),
      'entity_id' => $node->id(),
      'path' => $node->toUrl()->setAbsolute(FALSE)->toString(),
    ], 'nodes_inspections');
  }

  /**
   * Process node page.
   *
   * @throws \Drupal\Core\Entity\EntityMalformedException
   * @throws \Exception
   */
  protected function shouldTrackView(string $view_id, string $display_id): bool {
    $views_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('views_enabled');
    return in_array($view_id . '.' . $display_id, $views_enabled, TRUE);
  }

  /**
   * Checks if we should track a current node.
   */
  protected function shouldTrackNode(NodeInterface $node): bool {
    $node_types_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('content_types');
    return in_array($node->bundle(), $node_types_enabled, TRUE);
  }

  /**
   * Checks if we should track a current term.
   */
  protected function shouldTrackTerm(TermInterface $term): bool {
    $vocabularies_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('vocabularies');
    return in_array($term->bundle(), $vocabularies_enabled, TRUE);
  }

  /**
   * Checks if current route should be tracked.
   */
  protected function shouldTrackRoute(string $route_name): bool {
    $routes_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('routes');
    return in_array($route_name, $routes_enabled, TRUE);
  }

}
