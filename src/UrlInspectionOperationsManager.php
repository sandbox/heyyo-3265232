<?php

namespace Drupal\url_inspector;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\node\NodeInterface;
use Drupal\taxonomy\TermInterface;

/**
 * Contains methods to work with operations on UrlInspection entities.
 */
class UrlInspectionOperationsManager {

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * ConfigFactory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs UrlInspectionOperationsManager object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
  }

  /**
   * Returns last inspection entity.
   */
  public function getLastInspection(EntityInterface $inspected_entity): ?UrlInspectionInterface {
    $identifier = serialize([
      'entity_type_id' => $inspected_entity->getEntityTypeId(),
      'entity_id' => $inspected_entity->id(),
    ]);

    $inspector_entity = $this->entityTypeManager->getStorage('url_inspection')->loadByProperties([
      'entity_identifier' => $identifier,
    ]);

    if (!empty($inspector_entity)) {
      return reset($inspector_entity);
    }

    return NULL;
  }

  /**
   * Checks if inspection exists.
   */
  public function inspectionExists($inspection_identifier): bool {
    $result = $this->entityTypeManager->getStorage('url_inspection')->loadByProperties([
      'entity_identifier' => $inspection_identifier,
    ]);

    return !empty($result);
  }

  /**
   * Updates existing inspection.
   */
  public function updateInspection(array $data): void {
    $inspection_entity = $this->entityTypeManager->getStorage('url_inspection')->loadByProperties([
      'entity_identifier' => $data['entity_identifier'],
    ]);
    $inspection_entity = reset($inspection_entity);
    foreach ($data as $field => $value) {
      $inspection_entity->set($field, $value);
    }
    $inspection_entity->save();
  }

  /**
   * Creates inspection entity.
   */
  public function createInspection(array $data): void {
    $inspection = $this->entityTypeManager->getStorage('url_inspection')->create($data);
    $inspection->save();
  }

  /**
   * Check if we should track current view.
   *
   * @param string $view_id
   *   View ID.
   * @param string $display_id
   *   View display ID.
   *
   * @return bool
   *   TRUE if we should track this view.
   */
  public function shouldTrackView(string $view_id, string $display_id): bool {
    $views_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('views_enabled');

    if (empty($views_enabled)) {
      return FALSE;
    }

    return in_array($view_id . '.' . $display_id, $views_enabled, TRUE);
  }

  /**
   * Determines whether a given node should be tracked.
   *
   * @param \Drupal\node\NodeInterface $node
   *   The node to check.
   *
   * @return bool
   *   TRUE if the node should be tracked, FALSE otherwise.
   */
  public function shouldTrackNode(NodeInterface $node): bool {
    $node_types_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('content_types');

    if (empty($node_types_enabled)) {
      return FALSE;
    }

    return in_array($node->bundle(), $node_types_enabled, TRUE);
  }

  /**
   * Determines whether a given term should be tracked.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   The term to check.
   *
   * @return bool
   *   TRUE if the term should be tracked, FALSE otherwise.
   */
  public function shouldTrackTerm(TermInterface $term): bool {
    $vocabularies_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('vocabularies');

    if (empty($vocabularies_enabled)) {
      return FALSE;
    }

    return in_array($term->bundle(), $vocabularies_enabled, TRUE);
  }

  /**
   * Determines whether the current route should be tracked.
   *
   * @param string $route_name
   *   The name of the route to check.
   *
   * @return bool
   *   TRUE if the route should be tracked, FALSE otherwise.
   */
  public function shouldTrackRoute(string $route_name): bool {
    $routes_enabled = $this->configFactory
      ->get('url_inspector.settings')
      ->get('routes');

    if (empty($routes_enabled)) {
      return FALSE;
    }

    return in_array($route_name, $routes_enabled, TRUE);
  }

}
