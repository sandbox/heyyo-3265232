<?php

declare(strict_types=1);

namespace Drupal\url_inspector\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;

/**
 * Defines the URL Inspection type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "url_inspection_type",
 *   label = @Translation("URL Inspection type"),
 *   label_collection = @Translation("URL Inspection types"),
 *   label_singular = @Translation("url inspection type"),
 *   label_plural = @Translation("url inspections types"),
 *   label_count = @PluralTranslation(
 *     singular = "@count url inspections type",
 *     plural = "@count url inspections types",
 *   ),
 *   admin_permission = "administer url_inspection types",
 *   bundle_of = "url_inspection",
 *   config_prefix = "url_inspection_type",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "uuid",
 *   },
 * )
 */
final class UrlInspectionType extends ConfigEntityBundleBase {

  /**
   * The machine name of this url inspection type.
   */
  protected string $id;

  /**
   * The human-readable name of the url inspection type.
   */
  protected string $label;

}
