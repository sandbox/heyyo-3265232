<?php

declare(strict_types=1);

namespace Drupal\url_inspector\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\url_inspector\UrlInspectionInterface;

/**
 * Defines the url inspection entity class.
 *
 * @ContentEntityType(
 *   id = "url_inspection",
 *   label = @Translation("URL Inspection"),
 *   label_collection = @Translation("URL Inspections"),
 *   label_singular = @Translation("url inspection"),
 *   label_plural = @Translation("url inspections"),
 *   label_count = @PluralTranslation(
 *     singular = "@count url inspections",
 *     plural = "@count url inspections",
 *   ),
 *   bundle_label = @Translation("URL Inspection type"),
 *   handlers = {
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "url_inspection",
 *   admin_permission = "administer url_inspection types",
 *   entity_keys = {
 *     "id" = "id",
 *     "bundle" = "bundle",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   bundle_entity_type = "url_inspection_type",
 *   field_ui_base_route = "entity.url_inspection_type.edit_form",
 * )
 */
final class UrlInspection extends ContentEntityBase implements UrlInspectionInterface {

  use EntityChangedTrait;

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['label'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Label'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled');

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the url inspection was created.'));

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the url inspection was last changed.'))
      ->setTranslatable(TRUE);

    $fields['verdict_status'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Verdict Status'))
      ->setDescription(t('The verdict status'));

    $fields['crawled_date'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Crawled Date'))
      ->setDescription(t('The date/time when page/URL was visited by the Google crawler last time.'))
      ->setDefaultValue(0);

    $fields['detailed_info'] = BaseFieldDefinition::create('json')
      ->setLabel('Detailed Info')
      ->setDescription('String to store extended JSON data from the Google Search Console API');

    $fields['entity_identifier'] = BaseFieldDefinition::create('map')
      ->setLabel(t('Entity identifier'))
      ->setDescription(t('Stores an information about entity that was inspected via URL inspector.'));

    return $fields;
  }

}
