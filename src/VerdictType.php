<?php

namespace Drupal\url_inspector;

/**
 * Enum for supported verdict types.
 */
enum VerdictType: int {

  // Unknown verdict.
  case VERDICT_UNSPECIFIED = 0;

  // Equivalent to "Valid" for the page or item in the Search Console.
  case PASS = 1;

  // Reserved, no longer in use.
  case PARTIAL = 2;

  // Equivalent to "Error" or "Invalid" for the page in the Search Console.
  case FAIL = 3;

  // Equivalent to "Excluded" for the page or item in the Search Console.
  case NEUTRAL = 4;

  /**
   * Returns enum value by specified string name of the case.
   */
  public static function fromName(string $name): int {
    foreach (self::cases() as $verdict_type) {
      if ($name === $verdict_type->name) {
        return $verdict_type->value;
      }
    }
    throw new \ValueError("$name is not a valid backing value for enum " . self::class);
  }

  /**
   * Returns color specific to the verdict value.
   */
  public static function getColor(int $value): ?string {
    return match($value) {
      self::VERDICT_UNSPECIFIED->value => 'orange',
      self::PASS->value => 'green',
      self::PARTIAL->value => 'lightgray',
      self::FAIL->value => 'red',
      self::NEUTRAL->value => 'gray',
      default => NULL,
    };

  }

}
